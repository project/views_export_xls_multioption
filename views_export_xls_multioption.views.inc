<?php

/**
 * @file
 * Contains Views style plugin.
 */

/**
 * Implements hook_views_plugins().
 */
function views_export_xls_multioption_views_plugins() {
  $path = drupal_get_path('module', 'views_export_xls_multioption');
  return array(
    'style' => array(
      'views_data_export_xls_multiopion' => array(
        'title' => t('XLS file (every option in separate cell)'),
        'help' => t('Display the view as a xls file.'),
        'handler' => 'views_data_export_plugin_style_export',
        'path' => drupal_get_path('module', 'views_data_export') . '/plugins',
        'export headers' => array('Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'),
        'export feed type' => 'xlsx',
        'export feed text' => 'XLSX',
        'export feed file' => '%view.xlsx',
        'export feed icon' => drupal_get_path('module', 'views_data_export') . '/images/xls.png',
        'additional themes' => array(
          'views_export_xls_multioption_header' => 'style',
          'views_export_xls_multioption_body' => 'style',
          'views_export_xls_multioption_footer' => 'style',
        ),
        'additional themes base' => 'views_export_xls_multioption',
        'parent' => 'views_data_export',
        'theme' => 'views_export_xls_multioption',
        'theme path' => $path . '/theme',
        'theme file' => 'views_export_xls_multioption.theme.inc',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'data_export',
      ),
    ),
  );
}
