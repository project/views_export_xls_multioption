<?php

/**
 * @file
 *
 */

module_load_include('theme.inc', 'views_data_export', 'theme/views_data_export');

/**
 * Preprocess xls header.
 */
function template_preprocess_views_export_xls_multioption_header(&$vars) {
  // Pass through the generic MS Office preprocess.
  template_preprocess_views_data_export_msoffice_header($vars);
}

/**
 * Preprocess xls body.
 */
function template_preprocess_views_export_xls_multioption_body(&$vars) {
  _views_data_export_header_shared_preprocess($vars);
  _views_data_export_body_shared_preprocess($vars);

  $view = $vars['view'];

  $output = '';
  foreach ($vars['themed_rows'] as $number => $row) {

    // Check if we're dealing with a simple or complex row
    if (isset($row['data'])) {
      foreach ($row as $key => $value) {
        if ($key == 'data') {
          $cells = $value;
        }
      }
    }
    else {
      $cells = $row;
    }

    if (!empty($cells)) {

      $rows = array();
      $max = 0;
      foreach ($cells as $field_name => $field_value) {

        // Get field's cardinality to make sure it's not a single value field.
        $field = $view->field[$field_name];

        // Get cardinality from the field definition.
        if (!empty($field->real_field)) {
          $field_info = field_info_field($field->real_field);
          $cardinality = !empty($field_info['cardinality']) ? $field_info['cardinality'] : 1;
        }
        else {
          $cardinality = !empty($field->field_info['cardinality']) ? $field->field_info['cardinality'] : 1;
        }

        // Get separator of multiple field values.
        if (!empty($field->options['separator'])) {
          $separator = $field->options['separator'];
        }
        elseif (!empty($field->options['list']['separator'])) {
          $separator = $field->options['list']['separator'];
        }

        // If field cardinality is not "1" then there's something potentially to
        // explode.
        if (!empty($separator) && $cardinality != 1) {
          $field_value = htmlspecialchars_decode($field_value);
          $splited = explode($separator, $field_value);
        }
        else {
          $splited = array($field_value);
        }

        $cells[$field_name] = $splited;
        $max = max($max, count($splited));
      }

      foreach ($cells as $field_name => $field_values) {
        if (count($field_values) == 1) {
          $rows[0][$field_name] = array_shift($field_values);
        }
        else {
          for ($i = 0; $i < $max; $i++) {
            $rows[$i][$field_name] = isset($field_values[$i]) ? $field_values[$i] : '';
          }
        }
      }

      foreach ($rows as $key => $table_row) {
        $output .= '<tr>';
        foreach ($table_row as $field_name => $field_value) {
          $rowspan = !isset($rows[$key + 1][$field_name]) ? $max - $key : 1;
          $output .= $rowspan > 1 ? '<td rowspan="' . $rowspan .'">' : '<td>';
          $output .= $field_value;
          $output .= '</td>';
        }
        $output .= "</tr>\n";
      }
    }
  }

  $vars['tbody'] = preg_replace('/<\/?(a|span) ?.*?>/', '', $output); // strip 'a' and 'span' tags
}

/**
 * Converts HTML to XLs on the fly.
 */
function template_preprocess_views_export_xls_multioption(&$vars) {

  // Get html markup.
  $output = $vars['rows']['header'] . $vars['rows']['body'] . $vars['rows']['footer'];

  // Put html into a temporary file.
  $html_file_name = drupal_tempnam('temporary://', 'file');
  file_put_contents($html_file_name, $output);

  // Prepare file path for xls file.
  $outputFileType = 'Excel2007';
  $xls_file_name = drupal_tempnam('temporary://', 'file');
  $xls_file_name = drupal_realpath($xls_file_name);

  // Load external library which converts HTML to PHP.
  $library = libraries_load('PHPExcel');
  libraries_load_files($library);

  // Convert HTML to xls and save it into the file.
  $objPHPExcelReader = PHPExcel_IOFactory::createReader('HTML');
  @$objPHPExcel = $objPHPExcelReader->load($html_file_name);
  $objPHPExcelWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,$outputFileType);
  $objPHPExcelWriter->save($xls_file_name);

  // Get content of xls file.
  $vars['content'] = file_get_contents($xls_file_name);
}
